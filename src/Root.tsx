import CssBaseline from "@material-ui/core/CssBaseline"
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles"
import React, { FunctionComponent } from "react"
import {
    HashRouter as Router,
    Route,
    Switch
} from "react-router-dom"
import "typeface-roboto"
import { TopBar } from "./components/TopBar"
import { Contact } from "./pages/Contact"
import { Home } from "./pages/Home"
import { MiniApps } from "./pages/MiniApps"
import "./Root.scss"

const muiTheme = createMuiTheme({
    palette: {
        primary: {
            contrastText: "#ffffff",
            dark: "#540000",
            light: "#bd433a",
            main: "#870b13",
        }
    },
    typography: {
        button: {
            textTransform: "none"
        }
    }
})

export const Root: FunctionComponent<{}> = () => {
    return (
        <MuiThemeProvider theme={muiTheme}>
            <CssBaseline />
            <Router>
                <TopBar />
                <Switch>
                    <Route exact={true} path="/" component={Home} />
                    <Route exact={true} path="/home" component={Home} />
                    <Route exact={true} path="/contact" component={Contact} />
                    <Route path="/miniapps" component={MiniApps} />
                    <Route path="/miniapps/:appName" component={MiniApps} />
                </Switch>
            </Router>
        </MuiThemeProvider>
    )
}
