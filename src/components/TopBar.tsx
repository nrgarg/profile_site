import { AppBar, Button, IconButton, Toolbar } from "@material-ui/core"
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles"
import { Home } from "@material-ui/icons"
import * as React from "react"
import { useHistory } from "react-router"
import "./TopBar.scss"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(2),
      minWidth: 200
    },
    text: {
        color: "white",
        lineHeight: 1.6
    }
  })
)

export const TopBar: React.FunctionComponent<{}> = () => {
    const history = useHistory()

    const goToHome = () => history.replace("/")
    const goToContact = () => history.replace("/contact")
    const goToMiniApps = () => history.replace("/miniapps")

    return (
        <AppBar color="primary" position="sticky">
            <Toolbar>
                <IconButton color="inherit" aria-label="home" onClick={goToHome} data-testid="go-to-home">
                    <Home />
                </IconButton>
                <Button color="inherit" aria-label="contact" onClick={goToContact} data-testid="go-to-contact">
                    Contact Me
                </Button>
                <Button color="inherit" aria-label="miniapps" onClick={goToMiniApps} data-testid="go-to-miniapps">
                    Mini Apps
                </Button>
            </Toolbar>
        </AppBar>
    )
}
