import { Button, ButtonGroup } from "@material-ui/core"
import { Backspace, Delete } from "@material-ui/icons"
import { times } from "lodash"
import React, { FunctionComponent, useState } from "react"

export type KeyType = string | number

export interface IMiniKeyboardProps {
    keys: KeyType[]
    label: string
    gridSize?: number
    onSelect(input: KeyType): any
    onDelete(): any
    onBackspace(): any
}

export const MiniKeyboard: FunctionComponent<IMiniKeyboardProps> = ({ gridSize = 5, ...props }) => {
    const rows = Math.ceil(props.keys.length / gridSize)
    const buttonGroups = times(rows, (time) => {
        const subset = props.keys.slice(time * gridSize, (time + 1) * gridSize)
        const buttons = subset.map((key) => {
            const onSelect = React.useCallback(() => {
                props.onSelect(key)
            }, [key])
            return <Button key={`${props.label}-${time}-${key}`} fullWidth={true} onClick={onSelect}>{key}</Button>
        })

        return (
            <React.Fragment>
                <ButtonGroup size="small" fullWidth={false} variant="contained" key={`${props.label}-${time}`}>
                    {buttons}
                </ButtonGroup>

                <br />
            </React.Fragment>
        )
    })

    return (
        <div className="miniKeyboard">
            {buttonGroups}
            <ButtonGroup className="miniKeyboard_actions" variant="contained" size="small" color="primary">
                <Button className="miniKeyboard_actions--delete" onClick={props.onDelete}>
                    <Delete fontSize="small" />
                </Button>
                <Button className="miniKeyboard_actions--backspace" onClick={props.onBackspace}>
                    <Backspace fontSize="small" />
                </Button>
            </ButtonGroup>
        </div>
    )
}
