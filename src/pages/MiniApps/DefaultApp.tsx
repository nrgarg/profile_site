import { Button, Grid } from "@material-ui/core"
import * as React from "react"
import { useHistory } from "react-router"
import { useMiniApps } from "./"

export const DefaultApp = () => {
    const apps = useMiniApps()
    const history = useHistory()

    const items = apps.map((app) => {
        const goToApp = () => history.replace(`/miniapps/${app.path}`)
        return (
            <Grid item={true} xs={3} key={app.name} onClick={goToApp}>
                <Button>
                    {app.name}
                </Button>
            </Grid>
        )
    })
    return (
        <div>
            <h3>Choose a mini app!</h3>
            <Grid container={true} spacing={3}>
                {items}
            </Grid>
        </div>
    )
}
