import { Button, ButtonGroup } from "@material-ui/core"
import { isEmpty } from "lodash"
import * as React from "react"
import { characterCodes } from "./characterCodes"

export interface ICalculatorProps {
    nameCharacters: string[]
    trainerId: string
    money: string
}

export const getNumberCode = (input: number): number => {
    if (input > 65535) {
        return getNumberCode(input - (Math.floor(input / 65535) * 65535))
    }

    const quotient = Math.floor(input / 256)
    const remainder = input % 256

    return quotient + remainder
}

export const getNameCode = (input: string[]): number => {
    return input.slice(0, 4).reduce((acc, char) => acc + characterCodes.get(char), 0)
}

export const Results: React.FunctionComponent<{ label: string, value: string | number }> = (props) => {
    return (
        <p>
            Result for {props.label} = {props.value}
        </p>
    )
}

export const Calculator: React.FunctionComponent<ICalculatorProps> = (props) => {
    const { nameCharacters, money, trainerId } = props
    const [result, setResult] = React.useState()

    const calculateCode = React.useCallback(() => {
        const sum = [
            getNameCode(nameCharacters),
            getNumberCode(Number(trainerId)),
            getNumberCode(Number(money))
        ].reduce((acc, value) => acc + value, 0)

        setResult(sum)

    }, [setResult, nameCharacters, money, trainerId])

    return (
        <React.Fragment>
            <ButtonGroup variant="contained">
                <Button
                    color="primary"
                    onClick={calculateCode}
                    disabled={isEmpty(nameCharacters) || isEmpty(money) || isEmpty(trainerId)}
                >
                    Calculate Your Code!
                </Button>
            </ButtonGroup>
            {result != null && <h3>Your code = {result}</h3>}
        </React.Fragment>
    )
}
