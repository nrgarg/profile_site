import { Box, Grid, makeStyles, Tab, Tabs, Theme, Typography } from "@material-ui/core"
import * as React from "react"
import { MiniKeyboard } from "../../../components/MiniKeyboard"
import { Calculator } from "./Calculator"
import { lowerCase, special, upperCase } from "./characterCodes"

interface ITabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
  }

const TabPanel: React.FunctionComponent<ITabPanelProps> = (props) => {
    const { children, value, index, ...other } = props

    return (
        <Typography
          component="div"
          role="tabpanel"
          hidden={value !== index}
          key={`tabpanel-${index}`}
          id={`vertical-tabpanel-${index}`}
          aria-labelledby={`vertical-tab-${index}`}
          {...other}
        >
          {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    )
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        display: "flex",
        flexGrow: 1,
        height: 150,
        width: 600
    },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    }
}))

export const PokemonPasswordCalculatorApp: React.FunctionComponent<{}> = () => {
    const LINK_1 = "http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9mon_Gold_and_Silver_Versions"
    const LINK_2 = "http://www.gamefaqs.com/gbc/446340-pokemon-silver-version/faqs/12550"
    const [nameKeyboardTab, setNameKeyboardTab] = React.useState(0)
    const [money, setMoney] = React.useState("")
    const [name, setName] = React.useState<string[]>([])
    const [trainerId, setTrainerId] = React.useState("")
    const classes = useStyles({})

    const handleNameChange = React.useCallback((value: string) => {
        setName((chars) => chars.concat(value))
    }, [setName])

    const handleTrainerIdChange = React.useCallback((value: number) => {
        setTrainerId((trainerIdValue) => trainerIdValue + value.toString())
    }, [setTrainerId])

    const handleMoneyChange = React.useCallback((value: number) => {
     setMoney((moneyValue) => moneyValue  + value.toString())
    }, [setMoney, money])

    const handleNameTabChange = React.useCallback((event: React.ChangeEvent<{}>, newValue: number) => {
        setNameKeyboardTab(newValue)
    }, [setNameKeyboardTab])

    const handleNameBackspace = React.useCallback(() => {
        setName((chars) => chars.slice(0, chars.length - 1))
    }, [setName])
    const handleNameClear = React.useCallback(() => {
        setName(() => [])
    }, [setName])

    const handleMoneyBackspace = React.useCallback(() => {
        setMoney((moneyValue) => moneyValue.slice(0, moneyValue.length - 1))
    }, [setMoney])
    const handleMoneyClear = React.useCallback(() => {
        setMoney(() => "")
    }, [setMoney])

    const handleTrainerIdBackspace = React.useCallback(() => {
        setTrainerId((trainerIdValue) => trainerIdValue.slice(0, trainerIdValue.length - 1))
    }, [setTrainerId])
    const handleTrainerIdClear = React.useCallback(() => {
        setTrainerId(() => "")
    }, [setTrainerId])

    return (
        <React.Fragment>
            <h3>Pokemon Password Calculator</h3>
            <p>
                In <a href={LINK_1}>Pokemon Gold and Silver Versions</a>,
                there is a way to reset the date and time for your game - without have to reset the entire thing.
            </p>
            <p>
                The calculation is available <a href={LINK_2}>here</a>.
            </p>

            <p>
                Just provide your name, trainer ID and money into the fields below and you'll have your reset code!
            </p>
            <Grid container={true} spacing={3} direction="column">
                <Grid container={true} item={true} xs={12} direction="row">
                    <Grid item={true} xs={6} className="setName">
                        <h3>Name: {name.join("")}</h3>
                        <div className={classes.root}>
                            <Tabs
                                orientation="vertical"
                                className={classes.tabs}
                                value={nameKeyboardTab}
                                onChange={handleNameTabChange}
                            >
                                <Tab label="a-z" />
                                <Tab label="A-Z" />
                                <Tab label="Special" />
                            </Tabs>
                            <TabPanel value={nameKeyboardTab} index={0}>
                                <MiniKeyboard
                                    label="lowerCase"
                                    gridSize={9}
                                    keys={Object.keys(lowerCase)}
                                    onSelect={handleNameChange}
                                    onBackspace={handleNameBackspace}
                                    onDelete={handleNameClear}
                                />
                            </TabPanel>
                            <TabPanel value={nameKeyboardTab} index={1}>
                                <MiniKeyboard
                                    label="upperCase"
                                    gridSize={9}
                                    keys={Object.keys(upperCase)}
                                    onSelect={handleNameChange}
                                    onBackspace={handleNameBackspace}
                                    onDelete={handleNameClear}
                                />
                            </TabPanel>
                            <TabPanel value={nameKeyboardTab} index={2}>
                                <MiniKeyboard
                                    label="special"
                                    gridSize={9}
                                    keys={Object.keys(special)}
                                    onSelect={handleNameChange}
                                    onBackspace={handleNameBackspace}
                                    onDelete={handleNameClear}
                                />
                            </TabPanel>
                        </div>
                    </Grid>
                    <Grid item={true} xs={3} className="setTrainerId">
                        <h3>Trainer ID: {trainerId}</h3>
                        <MiniKeyboard
                            label="trainerId"
                            gridSize={3}
                            keys={[1, 2, 3, 4, 5, 6, 7, 8, 9, 0]}
                            onSelect={handleTrainerIdChange}
                            onBackspace={handleTrainerIdBackspace}
                            onDelete={handleTrainerIdClear}
                        />
                    </Grid>
                    <Grid item={true} xs={3} className="setMoney">
                        <h3>Money: {money}</h3>
                        <MiniKeyboard
                            label="money"
                            gridSize={3}
                            keys={[1, 2, 3, 4, 5, 6, 7, 8, 9, 0]}
                            onSelect={handleMoneyChange}
                            onBackspace={handleMoneyBackspace}
                            onDelete={handleMoneyClear}
                        />
                    </Grid>
                </Grid>
                <Grid item={true} xs={6}>
                    <Calculator nameCharacters={name} money={money} trainerId={trainerId} />
                </Grid>
            </Grid>
        </React.Fragment>
    )
}
